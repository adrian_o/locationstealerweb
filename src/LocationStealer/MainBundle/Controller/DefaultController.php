<?php

namespace LocationStealer\MainBundle\Controller;

use Ivory\GoogleMap\Overlays\Marker;
use Ivory\GoogleMap\Overlays\Polyline;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($udid)
    {
        $locations = $this->getDoctrine()->getRepository('LocationStealerMainBundle:Locations')
            ->createQueryBuilder('l')
                ->where('l.udid = :udid and l.createdAt BETWEEN :date_start AND :date_end')
                ->setParameters( array(
                    'udid' => $udid,
                'date_start' => new \DateTime('today'),
                'date_end'  => new \DateTime('tomorrow')
            ))
                ->orderBy('l.createdAt', 'ASC')
            ->getQuery()->getResult();

        $map = $this->get('ivory_google_map.map');
        $map->setStylesheetOptions(array(
            'width'  => '800px',
            'height' => '600px',
        ));
        $map->setAutoZoom(true);


        $polyline = new Polyline();

        $polyline->setPrefixJavascriptVariable('polyline_');

        $polyline->setOption('geodesic', true);
        $polyline->setOption('strokeColor', '#ffffff');
        $polyline->setOptions(array(
            'geodesic'    => true,
            'strokeColor' => '#ffffff',
        ));

        foreach($locations as $location){
            $marker = new Marker();

            $marker->setPosition($location->getLat(), $location->getLng(), true);
            $map->addMarker($marker);

            $polyline->addCoordinate($location->getLat(), $location->getLng(), true);
            $map->addPolyline($polyline);
        }

        return $this->render('LocationStealerMainBundle:Default:index.html.twig', array('map' => $map));
    }
}
