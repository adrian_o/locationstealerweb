<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EncodedPolyline
 *
 * @ORM\Table(name="ivory_encoded_polyline")
 * @ORM\MappedSuperClass
 */
class EncodedPolyline
{
    /**
     * @var string
     *
     * @ORM\Column(name="javascript_variable", type="string", length=100)
     */
    private $javascriptVariable;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text")
     */
    private $value;

    /**
     * @var array
     *
     * @ORM\Column(name="options", type="array")
     */
    private $options;


}
