<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ScaleControl
 *
 * @ORM\Table(name="ivory_scale_control")
 * @ORM\MappedSuperClass
 */
class ScaleControl
{
    /**
     * @var string
     *
     * @ORM\Column(name="control_position", type="string", length=100)
     */
    private $controlPosition;

    /**
     * @var string
     *
     * @ORM\Column(name="scale_control_style", type="string", length=100)
     */
    private $scaleControlStyle;


}
