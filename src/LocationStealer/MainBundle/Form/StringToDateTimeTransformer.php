<?php

namespace LocationStealer\MainBundle\Form;

use Symfony\Component\Form\DataTransformerInterface;


class StringToDateTimeTransformer implements DataTransformerInterface{
    public function transform($value)
    {

        if ( ! $value) {
            return null;
        }

        $date = new \DateTime();
        $date->setTimestamp($value);
        //$string = date_parse(date_format($value, "Y-m-d H:i:s"));
        $string = $date->format('Y-m-d H:i:s');
        if ( ! $date) {
            throw new TransformationFailedException(sprintf(
                'Cant transform date to string!',
                $value
            ));
        }

        return $string;
    }
    public function reverseTransform($value)
    {
        if ( ! $value) {
            return new \DateTime("now");
        }
        $date = new \DateTime();
        $date->setTimestamp($value);

        return $date;
    }
}
