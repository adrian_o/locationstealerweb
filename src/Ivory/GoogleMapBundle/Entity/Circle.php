<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Circle
 *
 * @ORM\Table(name="ivory_circle")
 * @ORM\MappedSuperClass
 */
class Circle
{
    /**
     * @var string
     *
     * @ORM\Column(name="javascript_variable", type="string", length=100)
     */
    private $javascriptVariable;

    /**
     * @var float
     *
     * @ORM\Column(name="radius", type="float")
     */
    private $radius;

    /**
     * @var array
     *
     * @ORM\Column(name="options", type="array")
     */
    private $options;


}
