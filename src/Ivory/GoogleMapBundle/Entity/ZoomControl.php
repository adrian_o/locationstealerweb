<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ZoomControl
 *
 * @ORM\Table(name="ivory_zoom_control")
 * @ORM\MappedSuperClass
 */
class ZoomControl
{
    /**
     * @var string
     *
     * @ORM\Column(name="control_position", type="string", length=100)
     */
    private $controlPosition;

    /**
     * @var string
     *
     * @ORM\Column(name="zoom_control_style", type="string", length=100)
     */
    private $zoomControlStyle;


}
