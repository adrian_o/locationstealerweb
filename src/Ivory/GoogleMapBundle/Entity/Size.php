<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Size
 *
 * @ORM\Table(name="ivory_size")
 * @ORM\MappedSuperClass
 */
class Size
{
    /**
     * @var string
     *
     * @ORM\Column(name="javascript_variable", type="string", length=100)
     */
    private $javascriptVariable;

    /**
     * @var float
     *
     * @ORM\Column(name="width", type="float")
     */
    private $width;

    /**
     * @var float
     *
     * @ORM\Column(name="height", type="float")
     */
    private $height;

    /**
     * @var string
     *
     * @ORM\Column(name="width_unit", type="string", length=20, nullable=true)
     */
    private $widthUnit;

    /**
     * @var string
     *
     * @ORM\Column(name="height_unit", type="string", length=20, nullable=true)
     */
    private $heightUnit;


}
