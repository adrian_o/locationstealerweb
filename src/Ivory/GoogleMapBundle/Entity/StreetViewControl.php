<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StreetViewControl
 *
 * @ORM\Table(name="ivory_street_view_control")
 * @ORM\MappedSuperClass
 */
class StreetViewControl
{
    /**
     * @var string
     *
     * @ORM\Column(name="control_position", type="string", length=100)
     */
    private $controlPosition;


}
