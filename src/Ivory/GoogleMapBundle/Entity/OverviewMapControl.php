<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OverviewMapControl
 *
 * @ORM\Table(name="ivory_overview_map_control")
 * @ORM\MappedSuperClass
 */
class OverviewMapControl
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="opened", type="boolean")
     */
    private $opened;


}
