<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoWindow
 *
 * @ORM\Table(name="ivory_info_window")
 * @ORM\MappedSuperClass
 */
class InfoWindow
{
    /**
     * @var string
     *
     * @ORM\Column(name="javascript_variable", type="string", length=100)
     */
    private $javascriptVariable;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=1000)
     */
    private $content;

    /**
     * @var array
     *
     * @ORM\Column(name="options", type="array")
     */
    private $options;

    /**
     * @var boolean
     *
     * @ORM\Column(name="open", type="boolean")
     */
    private $open;


}
