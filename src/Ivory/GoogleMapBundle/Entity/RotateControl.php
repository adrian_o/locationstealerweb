<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RotateControl
 *
 * @ORM\Table(name="ivory_rotate_control")
 * @ORM\MappedSuperClass
 */
class RotateControl
{
    /**
     * @var string
     *
     * @ORM\Column(name="control_position", type="string", length=100)
     */
    private $controlPosition;


}
