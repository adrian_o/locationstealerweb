<?php

namespace LocationStealer\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LocationsType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('udid')
            ->add('lat')
            ->add('lng')
            ->add(
                $builder->create('createdAt', 'text')
	                ->addModelTransformer(new StringToDateTimeTransformer() )
            )
        ;
    }

    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
		'data_class' => 'LocationStealer\MainBundle\Entity\Locations',
    'csrf_protection'   => false,		
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return '';
    }
}
