<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Map
 *
 * @ORM\Table(name="ivory_map")
 * @ORM\MappedSuperClass
 * @ORM\HasLifecycleCallbacks
 */
class Map
{
    /**
     * @var string
     *
     * @ORM\Column(name="javascript_variable", type="string", length=100)
     */
    private $javascriptVariable;

    /**
     * @var string
     *
     * @ORM\Column(name="html_container_id", type="string", length=255)
     */
    private $htmlContainerId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="auto_zoom", type="boolean")
     */
    private $autoZoom;

    /**
     * @var array
     *
     * @ORM\Column(name="map_options", type="array")
     */
    private $mapOptions;

    /**
     * @var array
     *
     * @ORM\Column(name="stylesheet_options", type="array")
     */
    private $stylesheetOptions;


    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        // Add your code here
    }
}
