<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bound
 *
 * @ORM\Table(name="ivory_bound")
 * @ORM\MappedSuperClass
 */
class Bound
{
    /**
     * @var string
     *
     * @ORM\Column(name="javascript_variable", type="string", length=100)
     */
    private $javascriptVariable;


}
