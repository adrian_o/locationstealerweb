<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MarkerShape
 *
 * @ORM\Table(name="ivory_marker_shape")
 * @ORM\MappedSuperClass
 */
class MarkerShape
{
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     */
    private $type;

    /**
     * @var array
     *
     * @ORM\Column(name="coordinates", type="array")
     */
    private $coordinates;


}
