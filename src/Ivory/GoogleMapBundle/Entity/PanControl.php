<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PanControl
 *
 * @ORM\Table(name="ivory_pan_control")
 * @ORM\MappedSuperClass
 */
class PanControl
{
    /**
     * @var string
     *
     * @ORM\Column(name="control_position", type="string", length=100)
     */
    private $controlPosition;


}
