<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MapTypeControl
 *
 * @ORM\Table(name="ivory_map_type_control")
 * @ORM\MappedSuperClass
 */
class MapTypeControl
{
    /**
     * @var array
     *
     * @ORM\Column(name="map_type_ids", type="array")
     */
    private $mapTypeIds;

    /**
     * @var string
     *
     * @ORM\Column(name="control_position", type="string", length=100)
     */
    private $controlPosition;

    /**
     * @var string
     *
     * @ORM\Column(name="map_type_control_style", type="string", length=100)
     */
    private $mapTypeControlStyle;


}
