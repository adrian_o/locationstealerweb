<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Table(name="ivory_event")
 * @ORM\MappedSuperClass
 */
class Event
{
    /**
     * @var string
     *
     * @ORM\Column(name="instance", type="string", length=100)
     */
    private $instance;

    /**
     * @var string
     *
     * @ORM\Column(name="event_name", type="string", length=50)
     */
    private $eventName;

    /**
     * @var string
     *
     * @ORM\Column(name="handle", type="string", length=1000)
     */
    private $handle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="capture", type="boolean", nullable=true)
     */
    private $capture;


}
