<?php

namespace Ivory\GoogleMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Polyline
 *
 * @ORM\Table(name="ivory_polyline")
 * @ORM\MappedSuperClass
 */
class Polyline
{
    /**
     * @var string
     *
     * @ORM\Column(name="javascript_variable", type="string", length=100)
     */
    private $javascriptVariable;

    /**
     * @var array
     *
     * @ORM\Column(name="options", type="array")
     */
    private $options;


}
