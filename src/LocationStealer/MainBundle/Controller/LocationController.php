<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 4/18/14
 * Time: 8:27 PM
 */

namespace LocationStealer\MainBundle\Controller;
use LocationStealer\MainBundle\Entity\Locations;
use LocationStealer\MainBundle\Form\LocationsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class LocationController extends Controller {

    public function postLocationAction(Request $request){

        $json = $request->getContent();
        $locations = json_decode($json, true);
        $id = array();
        if(isset($locations['locations']) && is_array($locations['locations'])){
            foreach( $locations['locations'] as $location ){
                $entity = new Locations();
                $entity->setLat($location['lat']);
                $entity->setLng($location['lng']);
                $entity->setUdid($location['udid']);
                $date = new \DateTime();
                $date->setTimestamp($location['createdAt']);
                $entity->setCreatedAt($date);

                $validator = $this->container->get('validator');
                $errors = $validator->validate($entity);

                if ( !count($errors) > 0) {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($entity);
                    $em->flush();

                    $id[] = $location['id'];
                }
            }
        }

        return array(
            'id' => $id,
        );

    }

    private function getErrorMessages(\Symfony\Component\Form\Form $form) {
        $errors = array();
        foreach ($form->getErrors() as $key => $error) {
            $errors[$key] = $error->getMessage();
        }
        if ($form->hasChildren()) {
            foreach ($form->getChildren() as $child) {
                if (!$child->isValid()) {
                    $errors[$child->getName()] = $this->getErrorMessages($child);
                }
            }
        }
        return $errors;
    }

} 